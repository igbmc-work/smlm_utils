# %%
# Import packages
from tifffile import imread
import numpy as np
import os
from glob import glob
from skimage.filters import gaussian, threshold_otsu
from skimage.transform import rescale
from rasterio.features import shapes

# %%
# Functions definition
def save_coords_from_nuclei_img(img_path):
    """
    This function reads nucleus image ROI, takes half of it (because it's doubled) 
    and finds edges of nucleus using gaussian blurring and thresholding. Then exports edges coordinates to txt file
    """
    path = img_path[:img_path.rfind('/')+1]
    img_name = img_path.split('/')[-1].replace('.tif','_ROI.txt')
    nuc_double = imread(img_path)
    nuc = nuc_double[:, :260]
    nuc_gauss = gaussian(nuc, sigma=2)
    nuc_binary = rescale(nuc_gauss > threshold_otsu(nuc_gauss), 100).astype(np.uint8)
    sh = shapes(nuc_binary)
    dic = next(sh)[0]   
    coords = np.array(dic['coordinates'][0])
    # swapped_coords = coords[:,[1,0]]
    np.savetxt(os.path.join(path,img_name), coords, comments='', header='1\nPolygonROI\n'+str(coords.shape[0]))

def main():
    """
    This is the batch processing program to process all subfolders in root
    """
    root = './'
    all_paths = [path for path in glob(root+"*/*/*") if os.path.isdir(path)]

    for path in all_paths:
        nuclei_path = [os.path.join(path,file) for file in os.listdir(path) if 'nucleus-roi' in file if file.endswith('.tif')]

        for nucleus_path in nuclei_path:
            coords_path = nucleus_path.replace('.tif', '_ROI.txt')
            if os.path.exists(coords_path):
                print('The coordinates of', coords_path, 'already exists')
            else:
                print("Processing", nucleus_path)
                save_coords_from_nuclei_img(nucleus_path)

# %%
# Start of program
if __name__ == "__main__":
    main()