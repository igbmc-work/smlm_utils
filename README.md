# SMLM utils
This is a collection of python utility scripts for handling and converting files across different formats.

## Folder structure
The following scripts are coded in a way to automatically loop over all subfolders of any given experiments. The ideal folder structure for the script to operate correctly is:

```
SMLM experiments
├── Exp1
│   ├── Cell1
|   |   ├── file.mat
|   |   └── nucleus-roi.tif
|   |
│   └── Cell2
|       ├── file.mat
|       └── nucleus-roi.tif
|   
└── Exp2
    ├── Cell1
    |   ├── file.mat
    |   └── nucleus-roi.tif
    |
    └── Cell(N)
        ├── file.mat
        └── nucleus-roi.tif
```

In this repo you will find the example-data folder which contains an example experiment and an example cell folders. To test the batch processing you can create extra copies of those folders (exp or cell) in the above described structure.

## Content
- convert_all_mat_to_csv.py &rarr; script to convert all Matlab mat files is all subfolder structure to csv with the correct heading and format to be imported into [Coloc Tesseler](https://github.com/flevet/Coloc-Tesseler).
- create_coordinates_for_color_tesseler.py &rarr; script to create a csv file containing Polygon coordinates needed to be imported by [Coloc Tesseler](https://github.com/flevet/Coloc-Tesseler) as ROI. These coordinates are generated from the corresponding fluorescence nucleus image using smoothing, thresholding and converting binary mask to polygon coordinates. This operation is performed in all subfolders containing nuclear images.

Both these scripts generate their output in **the same directory as the input**.

## Installation
1. Install [conda](https://docs.anaconda.com/anaconda/install/index.html)
2. Create conda virtual env using requirements.yml file
```
$ conda env create -f requirements.yml
```
3. Activate the env
```
$ conda activate smlm_utils
```
4. Use the python scripts while in the proper folder
```
$ python convert_all_mat_to_csv.py
$ python create_coordinates_for_color_tesseler.py
```